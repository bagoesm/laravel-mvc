@extends('adminlte.master')

@section('content')

<div class="card card-primary">

              <div class="card-header">
                <h3 class="card-title">Tambah Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/posts" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Judul Pertanyaan</label>
                    <input type="text" class="form-control" name="title" value="{{old('title', '')}}" id="title" placeholder="Enter Title">
                   @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  </div>
                  <div class="form-group">
                    <label for="body">Isi Pertanyaan</label>
                    <input type="text" class="form-control" id="body" name="body" value="{{old('body', '')}}" placeholder="Isi Pertanyaan">
                    @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  </div>
                  </div>
                  
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>


@endsection