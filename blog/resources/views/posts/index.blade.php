@extends('adminlte.master')

@section('content')

<div class="mt-3 ml-3">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Pertanyaan</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($posts as $key => $post)
                    <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{$post->title}}</td>
                    <td>{{$post->body}}</td>
                    <td>Action</td>
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            
            </div>

</div>

@endsection