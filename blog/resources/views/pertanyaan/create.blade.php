@extends('adminlte.master')

@section('content')

<div class="card card-primary mt-2 ml-2">

    <div class="card-header">
         <h3 class="card-title">Tambah Pertanyaan</h3>
    </div>
              <!-- /.card-header -->
              <!-- form start -->
    <form role="form" action="/pertanyaan" method="POST">
              @csrf
    <div class="card-body">
        <div class="form-group">
                    <label for="judul">Judul Pertanyaan</label>
                    <input type="text" class="form-control" name="judul" value="{{old('judul', '')}}" id="judul" placeholder="Enter Title">
                   @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
        </div>
        <div class="form-group">
                    <label for="isi">Isi Pertanyaan</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi', '')}}" placeholder="Isi Pertanyaan">
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
        </div>
    </div>
                  
                <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
    </form>
</div>
@endsection