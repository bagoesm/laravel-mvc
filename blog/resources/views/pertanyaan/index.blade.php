@extends('adminlte.master')

@section('content')

<div class="mt-3 ml-3">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div>
                <a href="/pertanyaan/create" class="btn btn-primary mb-2">Buat Pertanyaan</a>
                </div>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Pertanyaan</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($pertanyaan as $key => $post)
                    <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{$post->judul}}</td>
                    <td>{{$post->isi}}</td>
                    <td style="display: flex;"> <a href="/pertanyaan/{{$post->id}}" class="btn btn-info btn-sm">Show</a>
                    <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                    <form action="/pertanyaan/{{$post->id}}" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    
                    
                    </form>
                    
                    </td>
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            
            </div>

</div>

@endsection